function defaults(object, defaultProps) {

    if (typeof object === "object") {
    for(let key in defaultProps) {
        if(key in object) {
            object[key] = defaultProps[key];
        }
    }
    return object;
}else {
    return {}
}
}

module.exports = defaults;